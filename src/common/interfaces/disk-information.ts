export interface DiskInformation {
  config: string;
  data: string;
  stockage: string;
}
