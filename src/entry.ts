import 'reflect-metadata';
import 'core-js';

import { program } from '@caporal/core';
import { isElectron } from 'environ';
import packages from '../package.json';
import { BaseUpdater } from '@src/electron/update';
import { setupServer } from '@src/electron/server';
import { CliInformation } from '@common/interfaces/cli-information';
import { setStarterSetting } from '@common/helpers/starter-setting';

let settings: CliInformation;

program
  .name(packages.name)
  .version(packages.version)
  .strict(false)
  .argument('<env>', `Mode de l'application`, { default: 'PROD', validator: /PROD|DEV|LOCAL/ })
  .action(async ({ args }) => {
    settings.path = __dirname;
    settings.env = 'LOCAL';

    // Do not update when we are in local mode.
    if (!settings.local) {
      const update: any = await new BaseUpdater().checkForUpdate();
      if (update?.downloadPromise !== undefined) {
        const downloadPromise = update.downloadPromise;
        if (typeof downloadPromise === 'undefined') {
          return;
        }
      }
    }

    const { choosePortSync } = require('choose-port');
    settings.port = choosePortSync(settings.port, '127.0.0.1');

    settings = await setupServer(settings);

    if (isElectron()) {
      const { BaseApplication } = require('@src/electron');
      const baseApp = new BaseApplication();
      baseApp.createWindow(settings.debug, settings.web);
      (global as any).port = settings.port;
    }
  });

(async () => {
  settings = setStarterSetting();
  await program.run();
})();
