module.exports = {
  setupFilesAfterEnv: ['./tests/unit/jest.setup.ts'],
  moduleFileExtensions: ['js', 'vue', 'ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '.+\\.js$': 'babel-jest',
  },
  transformIgnorePatterns: ['node_modules/(?!(@ignition|vuex-class-modules)/)'],
  moduleNameMapper: {
    '@common/(.*)': '<rootDir>/src/common/$1',
    '@backend/(.*)': '<rootDir>/src/backend/$1',
    '@frontend/(.*)': '<rootDir>/src/frontend/$1',
    '@root/(.*)': '<rootDir>/$1',
    '@src/(.*)': '<rootDir>/src/$1',
  },
  testMatch: ['<rootDir>/tests/unit/**/*.spec.(ts|tsx)'],
  collectCoverage: false,
  coverageReporters: ['json', 'text-summary'],
  coverageDirectory: 'coverage-jest',
  collectCoverageFrom: ['./src/**/*.{js,vue,ts}', '!./node_modules/**'],
};
