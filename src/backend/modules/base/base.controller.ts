import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { PoolHelper } from '@src/thread/pool-helper';
import { Job } from '@src/thread/job';

@WebSocketGateway()
export class BaseController {
  @WebSocketServer()
  private readonly server!: Server;

  constructor() {
    PoolHelper.getInstance().createPool();

    // Send configuration to initially setup worker
    PoolHelper.getInstance().setupWorker();

    // Sent when all the worker are ready
    PoolHelper.getInstance().bindSetup(this.setup);

    // Sent when a setup fail (Manully declared in the worker
    PoolHelper.getInstance().bindSetupError(this.setupError);

    // Manully declared in the worker
    PoolHelper.getInstance().bindProgress(this.progress);

    // Manully declared in the worker (When a job is done to queue the next one)
    PoolHelper.getInstance().bindDone(this.done);

    // Sent when all the job are finished
    PoolHelper.getInstance().bindFinished(this.finished);
  }

  @SubscribeMessage('initialisation')
  public async init(@MessageBody() data: any, @ConnectedSocket() client: Socket): Promise<number> {
    PoolHelper.getInstance().addJob(new Job({ cmd: 'env', options: { bob: 1 } }));

    return data;
  }

  public setup(m: any): void {
    console.log(m);
  }

  public setupError(m: any): void {
    console.log(m);
  }

  public progress(m: any): void {
    console.log(m);
  }

  public done(m: any): void {
    console.log(m);
  }

  public finished(): void {
    console.log('finished');
  }
}
