import { Module } from '@nestjs/common';
import { BaseModule } from '@backend/modules/base/base.module';

@Module({
  imports: [BaseModule],
})
export class AppModule {}
