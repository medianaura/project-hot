module.exports = {
  extends: ['xo-vue'],
  extensions: ['vue'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  settings: {
    'import/resolver': {
      typescript: {},
    },
  },
  overrides: [
    {
      files: 'src/**/*.vue',
      rules: {
        '@typescript-eslint/consistent-indexed-object-style': 'off',
        'vue/html-indent': 'off',
        'vue/singleline-html-element-content-newline': 'off',
        'vue/script-indent': 'off',
        'vue/require-direct-export': 'off',
      },
    },
  ],
  rules: {
    '@typescript-eslint/comma-dangle': 'off',
    '@typescript-eslint/consistent-indexed-object-style': 'warn',
    '@typescript-eslint/indent': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/quotes': 'off',
    'arrow-parens': 'off',
    curly: 'off',
    'import/no-unassigned-import': 'off',
    indent: 'off',
    'new-cap': 'off',
    'object-curly-spacing': 'off',
  },
};
