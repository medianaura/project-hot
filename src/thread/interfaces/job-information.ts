export interface JobInformation {
  cmd: string;
  options: Record<string, any>;
}
