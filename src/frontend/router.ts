import Vue from 'vue';
import Router, { RouteConfig } from 'vue-router';
import Home from './pages/home.vue';

Vue.use(Router);

const allRoutes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
];

const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: allRoutes,
  linkActiveClass: 'is-active',
});

export default router;
