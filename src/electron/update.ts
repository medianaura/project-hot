const fs = require('fs');
const path = require('path');
const { app, dialog } = require('electron');

export class BaseUpdater {
  public autoUpdater: any;
  public interval: any;
  public onStart = true;

  public async checkForUpdate(): Promise<any> {
    const { autoUpdater } = require('electron-updater');

    this.autoUpdater = autoUpdater;

    const updateFilePath = path.resolve(app.getAppPath(), '../app-update.yml');
    if (!fs.existsSync(updateFilePath)) {
      return Promise.resolve(null);
    }

    this.autoUpdater.on('error', (error: any) => {
      dialog.showErrorBox('Error: ', error === null ? 'unknown' : (error.stack || error).toString());
    });

    this.autoUpdater.on('update-downloaded', this.update.bind(this));

    this.interval = setInterval(() => {
      this.onStart = false;
      this.autoUpdater.checkForUpdatesAndNotify();
    }, 60000);

    return this.autoUpdater.checkForUpdatesAndNotify();
  }

  public update(): void {
    clearInterval(this.interval);

    if (this.onStart) {
      setImmediate(() => this.autoUpdater.quitAndInstall());
      return;
    }

    dialog.showMessageBox(
      {
        title: 'Installation des mises à jour',
        message: "Mise a jour disponible. L'application va redémarrer pour les installés...",
        buttons: ['Oui', 'Non'],
      },
      (buttonIndex: number) => {
        if (buttonIndex === 0) {
          setImmediate(() => this.autoUpdater.quitAndInstall());
        }
      },
    );
  }
}
