import { Worker } from 'cluster';
import { EventEmitter } from 'eventemitter3';
import { Job } from './job';

const numberCPUs = require('os').cpus().length;
const cluster = require('cluster');

export class Pool extends EventEmitter {
  numCluster: number = numberCPUs;

  workers: Worker[] = [];

  idleWorkers: Worker[] = [];

  jobs: Job[] = [];

  isSetup = false;

  constructor(modulePath: string, threads: number = numberCPUs, options: string[] = []) {
    super();
    this.numCluster = threads;
    this.setupCluster(modulePath, options);

    cluster.on('message', (worker: Worker, message: any, handle?: any) => {
      if (!['progress'].includes(message.cmd.toLowerCase())) {
        this.idleWorkers.push(worker);
      }

      this.eventDispatcher(message);
      this.emit('p_handleJob');
    });

    this.on('p_handleJob', this.handleJob);
    this.on('p_remainingJob', this.remainingJob);
  }

  public reset(): void {
    this.workers.forEach((w) => {
      w.kill('0');
    });
  }

  public clearJob(): void {
    this.jobs = [];
  }

  public addJob(job: Job): void {
    this.jobs.push(job);
    this.emit('p_handleJob');
  }

  public setupWorker(options: any): void {
    if (this.idleWorkers.length !== this.numCluster) {
      throw new Error('Impossible de faire le setup. Worker in progress.');
    }

    this.isSetup = true;
    do {
      const worker = this.idleWorkers.shift();
      if (worker) {
        worker.send(options);
      }
    } while (this.idleWorkers.length > 0);
  }

  private handleJob(): void {
    if (this.jobs.length === 0 || this.idleWorkers.length === 0) return;

    const job = this.jobs.shift();
    const worker = this.idleWorkers.shift();

    if (job && worker) {
      job.workOn(worker);
    }
  }

  private remainingJob(): void {
    if (!(this.jobs.length === 0 && this.idleWorkers.length === this.numCluster)) {
      return;
    }

    this.emit('finished');
  }

  private eventDispatcher(message: any): void {
    if (this.isSetup) {
      if (this.idleWorkers.length !== this.numCluster) return;
      this.emit(message.cmd, message);
      this.isSetup = false;
    } else {
      this.emit(message.cmd, message);
      this.emit('p_remainingJob');
    }
  }

  private setupCluster(modulePath: string, options: string[]): void {
    cluster.setupMaster({ exec: modulePath, args: options });

    for (let index = 0; index < this.numCluster; index++) {
      const worker = cluster.fork();
      this.workers.push(worker);
      this.idleWorkers.push(worker);
    }
  }
}
