import parser from 'yargs-parser';
import { CliInformation } from '@common/interfaces/cli-information';

export function setStarterSetting(): CliInformation {
  let level = '0';

  // Activate verbose mode
  const argv = parser(process.argv);

  if (argv.quiet) {
    level = '4';
  }

  if (argv.v || argv.verbose) {
    level = '7';
  }

  let local = false;
  if (argv.local) {
    local = argv.local;
  }

  let web = false;
  if (argv.web) {
    web = argv.web;
  }

  let debug = false;
  if (argv.debug) {
    debug = argv.debug;
  }

  let port = 4001;
  if (argv.port) {
    port = argv.port;
  }

  return {
    level,
    local,
    web,
    debug,
    port,
    env: '',
    path: '',
  };
}
