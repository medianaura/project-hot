import { classToPlain, plainToClass } from 'class-transformer';
import { Default } from '@common/decorators/default';
import { DiskInformation } from '@common/interfaces/disk-information';

export class Configuration {
  @Default({ config: '', data: '' })
  public disk: DiskInformation = { config: '', data: '', stockage: '' };

  public env!: string;

  public version!: string;

  // TODO : Change the path to the config file
  public configPath = '%(disk_config)s\\_programmes\\Utilitaires\\Ultron\\Config\\';

  public static fromJSON(payload: Record<string, unknown>): Configuration {
    return plainToClass(Configuration, payload);
  }

  public toJSON(): Record<string, unknown> {
    return classToPlain(this);
  }
}
