const { app, BrowserWindow, dialog } = require('electron');
const path = require('path');
const url = require('url');
const debugFeature = require('electron-debug');
const p = require('@root/package.json');

export class BaseApplication {
  private win: any;

  constructor() {
    app.on('window-all-closed', () => {
      if (process.platform !== 'darwin') {
        app.quit();
      }
    });
  }

  public createWindow(debug: boolean, web: boolean): void {
    app.whenReady().then(() => {
      const config = Object.assign(
        {
          width: 1230,
          height: 850,
          title: p.config.title,
          show: false,
          resizable: false,
          icon: 'M:\\RxvTravail\\Images\\icones\\vigilance-sante.ico',
          webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
          },
        },
        p.config.electron,
      );
      // Create the browser window.
      this.win = new BrowserWindow(config);

      let urlPrecursor = {
        pathname: path.resolve(__dirname, '../frontend/', 'index.html'),
        protocol: 'file:',
        slashes: true,
      };

      if (web) {
        urlPrecursor = {
          pathname: 'localhost:3000',
          protocol: 'http:',
          slashes: true,
        };
      }

      // And load the index.html of the app.
      this.win.loadURL(url.format(urlPrecursor));

      this.win.once('ready-to-show', () => {
        if (debug) {
          debugFeature();
          this.win.webContents.openDevTools({ mode: 'detach' });
        }

        this.win.show();
      });

      this.win.on('closed', () => {
        process.exit(0); // eslint-disable-line unicorn/no-process-exit
      });
    });
  }
}
