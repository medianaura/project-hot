import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { InjectFromContainer } from 'di-manager';
import { container } from 'tsyringe';
import { ConfigurationService } from '@common/services/configuration';
import { CliInformation } from '@common/interfaces/cli-information';

@Controller()
export class AppController {
  @InjectFromContainer('ConfigurationService')
  private readonly configurationService!: ConfigurationService;

  constructor(private readonly appService: AppService) {
    this.configurationService.setInformation(container.resolve<CliInformation>('info'));
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
