!macro customInstall
    ${ifNot} ${isUpdated}
        CreateShortCut "$desktop\Electron Boilerplate.lnk" "$INSTDIR\Electron Boilerplate.exe" "LOCAL --local"
    ${endIf}

    ${ifNot} ${Silent}
    ${orIf} ${isForceRun}
        ExecShell "" "$INSTDIR\Electron Boilerplate.exe" "LOCAL --local"
    ${endIf}

    !undef RUN_AFTER_FINISH
!macroend

!macro customUnInstall
    ${ifNot} ${isUpdated}
        Delete "$desktop\Electron Boilerplate.lnk"
    ${endIf}
!macroend
