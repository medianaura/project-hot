import 'reflect-metadata';
import 'core-js';

import { container } from 'tsyringe';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IoAdapter } from '@nestjs/platform-socket.io';

require('dotenv-flow').config();

async function bootstrap(port: number) {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useWebSocketAdapter(new IoAdapter(app));
  await app.listen(port, '127.0.0.1');
  console.log(`Running on ${await app.getUrl()}`);
}

process.on('message', async (message) => {
  if (message.message !== 'information') {
    return;
  }

  container.register('info', { useValue: message.data });
  await bootstrap(message.data.port);

  // @ts-expect-error
  process.send('started_server');
});
